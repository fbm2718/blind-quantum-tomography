# -*- coding: utf-8- -*-
"""
This script runs a numerical recovery simulation using the
Sparse-Demixing-Tomography algorithm and for comparison a standard
tomography algorithm. The numerical data is stored in .h5py files and
then processed. The processed data is then displayed in a figure with
the signal reconstruction error versus the amount of used measurements. An
inset plot shows the reconstruction error of the calibration coefficients.

The figure is used in the numerical study in

    Roth, Wilkens, Hangleiter and Eisert. 
    "Semi-device-dependent blind quantum tomography" 
    https://arxiv.org/abs/2006.03069

Authors: 
    Jadwiga Wilkens, Ingo Roth and Dominik Hangleiter


Licence: 
    This project is licensed under the MIT License -
    see the LICENSE.md file for details.
"""

from Measurement import *
from SDTandALS import *
from csSignal import *
from plotFunctions import *
from ProcessData import *

# Initiate the dictionary for parameters regarding the
# signal and the measurement
sigMeasDict = {}
sigMeasDict["N"], sigMeasDict["dim"] = 10, 8
sigMeasDict["rank"], sigMeasDict["sparsity"] = 1, 4
firstlistM = np.arange(1, 160, 5)
secondlistM = np.arange(160, 270, 10)
sigMeasDict["listM"] = list(np.append(figureSize, secondlistM))
# Possible values for whichMType
# "MultiMeasurementGauss"
# "MultiMeasurementPauli"
# "NCalibrationSettingMMP"
# "FifthsBlockMMP"
# "NCorrectionBlockDiscriminationMMP"
# "CalibrationMMP"
# "CalibrationWithoutIdentity"
sigMeasDict["whichMType"] = "MultiMeasurementPauli"
sigMeasDict["calibCoeffMultiply"] = .05
sigMeasDict["calibCoeffOffset"] = .1
# Initiate the dictionary for parameters regarding the
# algorithm
algParDict = {}
algParDict["it"] = 1000
algParDict["thresh"] = 1e-5
algParDict["runs"] = 30
algParDict["repetitions"] = 1
# Could be "SDT", "demixingT", "cheatDemixingT", "ALS"
algParDict["listTomographyTypes"] = ["SDT"]
algParDict["numberOfShots"] = 10**8
# Initiate the dictionary for parameters regarding the
# boolean parameters
boolDict = {}
boolDict["initializationHack"] = False
boolDict["calibration"]        = True
boolDict["compareWStandardT"]  = True
boolDict["storeFiles"]         = True
boolDict["labBook"]            = False
boolDict["printVariables"]     = False
boolDict["randomTargetCoeff"]  = False
boolDict["firstTraceOne"]      = False
boolDict["allTraceOne"]        = False
# If required the ALS algorithm needs some extra parameters
ALSDict = {}
# The function prearrangement runs everything,
# meaning that here the actual simulation is happening,
# and returns the directory in which the numerical data is stored
dirName = prearrangement(sigMeasDict, algParDict, boolDict, ALSDict)
# Write the relevant recovery parameters in a .txt file
# With the name str:storeFileName plus the tomography type ending
storeFileName = dirName + "randomPauliN10dim8r1s4"
storeRelevantParametersInTxtFile(dirName, storeFileName)
# Build the file name for SDT and standardT
nameSDT = storeFileName + "SDT.txt"
nameCWST = storeFileName + "compareWStandardT.txt"
# Plot the data
figureSize = (11, 4.75)
plt.figure(figsize = figureSize)
plotNumericalSimulationSDTandStandardTomography(
                                            nameSDT,
                                            nameCWST,
                                            sigMeasDict["calibCoeffOffset"])
# And save it
plt.savefig(storeFileName + ".png")
# And don't forget to show the plot
plt.show()
