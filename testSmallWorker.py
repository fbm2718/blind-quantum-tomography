# -*- coding: utf-8- -*-
"""
This script runs a numerical recovery simulation using the
Sparse-Demixing-Tomography algorithm and for comparison a standard
tomography algorithm. The numerical data is stored in .h5py files and
then processed. The processed data is then displayed in a figure with
the signal reconstruction error versus the amount of used measurements. An
inset plot shows the reconstruction error of the calibration coefficients.

The figure can be used to test the code used for 

    Roth, Wilkens, Hangleiter and Eisert. 
    "Semi-device-dependent blind quantum tomography" 
    https://arxiv.org/abs/2006.03069

Authors: 
    Jadwiga Wilkens, Ingo Roth and Dominik Hangleiter


Licence: 
    This project is licensed under the MIT License -
    see the LICENSE.md file for details.
"""

from Measurement import *
from SDTandALS import *
from csSignal import *
from plotFunctions import *
from ProcessData import *

# Initiate the dictionary for parameters regarding the
# signal and the measurement
sigMeasDict = {}
sigMeasDict["N"], sigMeasDict["dim"] = 3, 4
sigMeasDict["rank"], sigMeasDict["sparsity"] = 1, 2
sigMeasDict["listM"] = list(np.arange(19, 21))
# Possible values for whichMType
# "MultiMeasurementGauss"
# "MultiMeasurementPauli"
# "NCalibrationSettingMMP"
# "FifthsBlockMMP"
# "NCorrectionBlockDiscriminationMMP"
# "CalibrationMMP"
# "CalibrationWithoutIdentity"
sigMeasDict["whichMType"] = "MultiMeasurementGauss"
sigMeasDict["calibCoeffMultiply"] = 1.
sigMeasDict["calibCoeffOffset"] = 0.
# Initiate the dictionary for parameters regarding the
# algorithm
algParDict = {}
algParDict["it"] = 600
algParDict["thresh"] = 1e-5
algParDict["runs"] = 1
algParDict["repetitions"] = 1
# Could be "SDT", "demixingT", "cheatDemixingT", "ALS"
algParDict["listTomographyTypes"] = ["SDT", "demixingT", "cheatDemixingT"]
algParDict["numberOfShots"] = False
# Initiate the dictionary for parameters regarding the
# boolean parameters
boolDict = {}
boolDict["initializationHack"] = False
boolDict["calibration"]        = False
boolDict["compareWStandardT"]  = False
boolDict["storeFiles"]         = True
boolDict["labBook"]            = False
boolDict["printVariables"]     = False
boolDict["randomTargetCoeff"]  = False
boolDict["firstTraceOne"]      = False
boolDict["allTraceOne"]        = False
# If required the ALS algorithm needs some extra parameters
ALSDict = {}
# The function prearrangement runs everything,
# meaning that here the actual simulation is happening,
# and returns the directory in which the numerical data is stored
dirName = prearrangement(sigMeasDict, algParDict, boolDict, ALSDict)
# Write the relevant recovery parameters in a .txt file
# With the name str:storeFileName plus the tomography type ending
storeFileName = dirName + "TESTgaussianN10dim16r1s3"
storeRelevantParametersInTxtFile(dirName, storeFileName)
# Build the file name for SDT and standardT
fileNameSDT = storeFileName + "SDT.txt"
fileNameDT = storeFileName + "demixingT.txt"
fileNamecheatingDT = storeFileName + "cheatDemixingT.txt"
print(fileNameSDT)

# Plot the data
figureSize = (5, 4.75)
plt.figure(figsize = figureSize)
plotDTcheatingDTandSDTProbabilityOfRecovery(fileNameDT,
                                            fileNamecheatingDT,
                                            fileNameSDT)
# And save it
plt.savefig(storeFileName + ".png")
# And don't forget to show the plot
plt.show()