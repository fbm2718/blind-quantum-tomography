# Blind quantum tomography, Algortihms v1

Blind quantum tomography aims at estimating low-rank quantum states under only mild assumptions on the calibration accuracy of the measurement devices.

As a special case, it includes a state-of-the-art implementation of a geometric optimisation for low-rank quantum state-tomography, sparse de-mixing of low-rank matrices and more. 

For more information we refer to 

* Roth, Wilkens, Hangleiter, and Eisert. *Semi-device-dependent blind quantum tomography*
	[https://arxiv.org/pdf/2006.03069.pdf](https://arxiv.org/pdf/2006.03069.pdf)

Beyond flexible implementation of the algorithms developed in the publication, this repository provides the entire 
code used for the publications' numerical studies.



## Requirements

The code is developed with Python 3.6.7. and uses the following fairly standard packages in some recent version: 
`numpy, scipy, matplotlib, copy, itertools, warnings, os, multiprocessing`.

## Demo 

A simple example running multiple reconstructions of small parameter values is provided in `testSmallWorker.py`
Run and wait for a few seconds. 
```
	testSmallWorker.py
```
This should display one plot displaying the reconstruction result.

## Reproducing the plots of the publication 
There are five figures in the publication which were produced using this library. Here is a list of the figures and the script producing it. Since there are many data points with a long runtime, please make sure that the device running the simulations is suited for this kind of workload. The figure will not turn out identical since there are still random numbers determining the instances.

Figure 4: Python script gaussSDTinformedDTandDT.py,

Figure 5: Python script randomPauliWorkerSparsity3.py,

Figure 6: Python script randomPauliWorkerSparsity4.py,

Figure 7: coherentPauliFlipSparsity2.py,

Figure 8: coherentPauliFlipSparsity3.py. 


## Contributing

We invite contributions to our code base in order to improve the package. Please contact us
via mail. 


## Authors

**Jadwiga Wilkens**, **Dominik Hangleiter** and **Ingo Roth** 


## How to cite 

Over and above the legal restrictions imposed by the license, if you use this software for an academic publication then you are obliged to provide proper attribution to the paper that describes it 

* Roth, Wilkens, Hangleiter, and Eisert. *Semi-device-dependent blind quantum tomography*
	[https://arxiv.org/pdf/2006.03069.pdf](https://arxiv.org/pdf/2006.03069.pdf)

and this code directly 

* Wilkens, Hangleiter, Roth: ***blind quantum tomography** -- Algorithms, v1 (2020). [https://gitlab.com/wilkensJ/blind-quantum-tomography](https://gitlab.com/wilkensJ/blind-quantum-tomography).

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.
