
# -*- coding: utf-8- -*-
"""

This class produces signal objects with which the recovery algorithms in
SDTandALS.py are run. It provides several methods example making a block signal
blockwise sparse or hard thresholding the rank of the several blocks

The class is used in the numerical study in

    Roth, Wilkens, Hangleiter and Eisert. 
    "Semi-device-dependent blind quantum tomography" 
    https://arxiv.org/abs/2006.03069

Authors: 
    Jadwiga Wilkens, Ingo Roth and Dominik Hangleiter


Licence: 
    This project is licensed under the MIT License -
    see the LICENSE.md file for details.
"""
import numpy as np
from scipy.sparse import *
from scipy.linalg import *
import random
import pdb


class MultiMyMatrix():
    """
    matrix out of matrices
    block wise matrix
    Attributes:
            int:dim      dimension of single matrix
            int:amount   how many single matrices are there
            int:rank    
            int:sparsity 
            np.ndarray:matrices [array(amount,dim,dim)] stores matrices in
                                                        one big matrix

    Methods:
            <<get/set>> multiMatrix
            <<get>> blockDiag
            <<set/set>> multiVector
            HardThresholdingRank()
            makeMultiSignalSparsed()
            <<get>> fullFrobArray
    """

    def __init__(self, amount, dim, rank=None, sparsity=None):
        self.dim      = dim
        self.amount   = amount
        if rank is None:
            rank=dim
        self.rank     = rank
        if sparsity is None:
            sparsity = amount
        self.sparsity = sparsity
        self.matrices = np.array([])

    @property
    def multiMatrix(self):
        """
        returns [array(amount,dim,dim)] matrixForm of MySignal: dim x dim
        """
        return self.matrices

    @multiMatrix.setter
    def multiMatrix(self,inputMatrix):
        self.matrices =  inputMatrix

    @property
    def blockDiag(self):
        new = block_diag(*self.matrices)
        return new

    @property
    def multiVector(self):
        new = np.transpose(self.matrices, (0, 2, 1))
        return new.reshape(self.amount * self.dim**2, 1)

    @multiVector.setter
    def multiVector(self, inputVector):
        """
        input [array(amount*dim**2,1)]
        Sets self._matrix through given (block) column vector with
        dimension (amount*dim**2,1),
        to matrix with dimension (amount*dim,dim).
        First column vector gets split into amount times column vectors
        second each column vector gets converted into a matrix by rewriting
        the columns into dim times columns,
        putting them next to each other in one matrix and transposing that
        now there are amount times matrices written in one column

        """
        gMatricesT = inputVector.reshape(self.amount, self.dim, self.dim)
        gMatrices = np.transpose(gMatricesT, [0, 2, 1])
        self.matrices = gMatrices

    @property
    def fullFrobArray(self):
        """
        return [array(1,amount)] array filled with frobenius norm of
                                 csSignal elements
        order is not changed of frobenius norm of single matrices
        """
        normArray = np.linalg.norm(self.matrices, axis=(1,2))
        return normArray

    def HardThresholdingRank(self, hermitian=False):
        """
        sets all but int[rank] biggest singular values to zero
        """
        if self.amount==1:
            U, singValues, Vdagg = np.linalg.svd(self.matrices[0])
            singValues[self.rank:] = 0
            sigma = np.eye(self.dim)*singValues
            self.matrices = np.dot(U,np.dot(sigma,Vdagg)).reshape(1,
                                                                  self.dim,
                                                                  self.dim)
        else:
            print("only working with self.amount=1!")



    def makeMultiSignalSparsed(self):
        """
        input:  sparse [int] sparsity of MulticsSignal
                        traceOne [boolean] if true, the trace of all
                                            csSignals are set to 1
                        support [array/boolean] csSignals are set to zero
        return: support (support of given signals)
        if support unknown:
                - array of frobenius norm gets sorted
                - all but int[sparse] biggest matrices set to zero
        if trace One:
                norm of remaining singular value arrays which are not 
                zeros get set to one
        """

        # if support==False, the sparsity is not known
        # if support==True, the sparsity is known but the support not,
        # set support=[] to go into second next elfi
        if self.dim==1:
            frobArray = self.matrices.flatten()
            sortedindex = np.argsort(abs(frobArray))
            sortedindex = sortedindex[::-1]
            notSup = sortedindex[self.sparsity:]
            for j in notSup:
                frobArray[j] = 0
            self.matrices = frobArray.reshape(self.amount,1,1)


class MulticsSignal():
    """
    stores multiples objects of csSignal in one ordered list
    possible to change single objects in list

    Attributes:
    hermitian=True
    int:amount
    int:dim
    int:rank
    int:sparsity
    np.ndarray:fullU
    np.ndarray:fullVdagg
    np.ndarray:fullSingValues


    Methods:
            constructRandom()
            drawRandUnitary()
            drawRandSingularValues()
            <<get/set>> multiVector 
            <<get/set>> frobNormArray
            makeMultiSignalSparsed(sparse,notsup=[],traceOne=False)
    """

    def __init__(self, amount, dimension, rank, sparsity,
                 hermitian=True, init_Random=False):
        self.amount = amount
        self.dim = dimension
        self.rank = rank
        self.sparsity = sparsity
        self.fullU = np.array([])
        self.fullVdagg = np.array([])
        self.fullSingValues = np.array([])
        self.hermitian = hermitian
        if init_Random:
            self.initRandom()

    def initRandom(self):
        """
        sets randomly self.SingularValues [array(1,rank*amount)],
        self.unitaryU [array(dim*amount,rank)] and
        self.unitaryV [array(dim*amount,rank)]
        if self.hermitian == True, self.V = self.U
        This is the SVD of bigcsSignal
        """
        unitaryU = self.drawRandUnitary()
        ones = np.ones(self.amount)[::, None]  # transpose it to column vector
        self.fullU = np.kron(ones, unitaryU).reshape(
            self.amount, self.dim, self.dim)
        if not self.hermitian:
            V = self.drawRandUnitary()
            self.fullVdagg = np.kron(ones, V)
        svalues = np.sort(abs(np.random.randn(1, self.rank)))[:, ::-1]
        svalues = svalues / np.linalg.norm(svalues)
        self.fullSingValues = np.kron(ones, svalues)

    def drawRandUnitary(self):
        """
        return [array (dim,dim)] a Haar random unitary matrix of size dim.
        """
        U = np.random.normal(0, 1, [self.dim, self.dim]) + \
            1j * np.random.normal(0, 1, [self.dim, self.dim])
        U, R = np.linalg.qr(U)
        D = np.diag(R)
        L = np.diag(np.divide(D, np.abs(D)))
        U = U.dot(L)
        # print(U)
        return U

    def drawRandSingularValues(self, normed=True):
        """
        return [array (amount,rank)] sorted in every rank long section
        (biggest first) array with rank many positiv entries
        if normed: ||svalues[:rank]||,||svalues[rank:2*rank]||, ... =1
        """
        svalues = np.sort(abs(np.random.randn(self.amount, self.rank)))[
            :, ::-1]
        if normed:
            normArray = np.sum(np.abs(svalues)**2, axis=-1)**(0.5)
            svalues = svalues / normArray[::, None]
        return svalues

    @property
    def multiMatrix(self):
        """
        Return [3Darray (amount,dim,dim)] matrix with amount times matrices
        in it (the signals stacked over each other).
        First write the singular values onto the diagonals of all amount
        times matrices,
        for that single singular value vectors must be dim times long!
        Not only rank times.
        Now the diagonal of the amount times dimxdim matrices are set to the
        singular values.
        Now there are amount times dimxdim matrices with the singular values
        of each SVD of each signal written onto the diagonal.
        For hermitian matrix fullVdagg is set to self.fullU.T.conj()
        (the .T is more complicated in the code).
        the np.matmul multiplies the matrices like the left matrix is a
        block matrix
        """
        # set fullVdagg:
        if self.hermitian:  # fullU*singvalues*fullUdagg
            fullVdagg = np.transpose(self.fullU, (0, 2, 1)).conj()
        else:
            fullVdagg = self.fullVdagg
        if len(self.fullSingValues[0]) != self.dim:
            zeros = np.zeros((self.amount, self.dim - self.rank))
            filledSvalues = np.append(self.fullSingValues, zeros, axis=1)
        else:
            filledSvalues = self.fullSingValues

        # set diag(*fullSingValues):
        # initiate amount times matrices filled with zeros
        diagSvalues = np.zeros((self.amount, self.dim, self.dim))
        # fill the diagonals with singular values each
        np.einsum('jii->ji', diagSvalues)[:] = filledSvalues

        # now multiply everything:
        # like 1 tensor fullU * 1 tensor diag(fullSingValues) * fullVdagg
        # For the first step, where fullU and fullVdag is not yet restricted,
        # restrict it by  hand
        rightSideMultiplikationMatrix = np.matmul(
                                        diagSvalues[:,:self.rank,:self.rank],
                                        fullVdagg[:,:self.rank])
        multiMatrix = np.matmul(self.fullU[:,:,:self.rank],
                                rightSideMultiplikationMatrix)
        return multiMatrix

    @multiMatrix.setter
    def multiMatrix(self, input):
        """
        input: [array(amount,dim,dim)] matrices in matrix
        sets fullU, fullSingValues and fullVdagg
        first check type, matrices in matrix is needed
                if matrices in matrix : set givenMatrices to input
                else: something went wrong.
        now do svd of every single matrix in big matrix 
        if hermitian we create an hermitian (X+Xdagg)/2 matrix and the 
            fullVdagg stays empty
        if not hermitian the fullVdagg gets set too
        """
        givenMatrices = np.array([])

        # check if type and shape is good:
        if type(input) == np.ndarray and \
                            input.shape == (self.amount, self.dim, self.dim):
            givenMatrices = input
        else:
            print("something wrong, shape of given array: " +
                  str(input.shape) + ", but we need: " + str(self.amount) +
                  ", " + str(self.dim)+ ", " + str(self.dim))

        # make the given matrix hermitian if self.hermitian == True:
        if self.hermitian:
            givenMatrices = (
                givenMatrices + np.transpose(givenMatrices,
                                             (0, 2, 1)).conj()) / 2

        # np.linalg.svd does amount times the svd of the single matrices 
        # (no for loop needed!)
        fullU, fullSingValues, fullVdagg = np.linalg.svd(givenMatrices)
        self.fullU = fullU
        self.fullSingValues = fullSingValues
        if not self.hermitian:
            self.fullVdagg = fullVdagg

    @property
    def multiVector(self):
        """
        all rows of all matrices written in one column vector
        """
        return np.transpose(self.multiMatrix,
                            (0, 2, 1)).reshape(self.amount * self.dim**2, -1)

    @multiVector.setter
    def multiVector(self, inputVector):
        gMatricesT = inputVector.reshape(self.amount, self.dim, self.dim)
        gMatrices = np.transpose(gMatricesT, [0, 2, 1])
        self.multiMatrix = gMatrices

    @property
    def fullFrobArray(self):
        """
        return [array(1,amount)] array filled with frobenius norm of
                                 csSignal elements
        order is not changed of frobenius norm of single matrices
        """
        normArray = np.linalg.norm(self.fullSingValues, axis=1)
        return normArray

    def projectTangentspace(self, input):
        """
        return matrix [array(dimension,dimension)] which was projected
                        onto Tangentspace of self.matrixForm
        first: get projecotrs onto self.U and self.V
        if input is of type MyMatrix, the matrixForm is needed
        or it is already a matrix
        then matrix gets projected
        """
        # If there is more than 1 dimension, project
        if self.dim != 1:
            pU = np.matmul(self.fullU, np.transpose(self.fullU, 
                                                    (0, 2, 1)).conj())
            if self.hermitian:
                pV = pU
            else:
                pV = np.matmul(np.transpose(
                    self.fullV, (0, 2, 1)).conj(), self.fullVdagg.T)
            oneIdentity = np.eye(self.dim)
            manyIdentities = np.tile(oneIdentity, (self.amount, 1)).reshape(
                self.amount, self.dim, self.dim)
            pUo, pVo = manyIdentities - pU, manyIdentities - pV

            givenMatrix = np.array([])

            if type(input) == MultiMyMatrix:
                givenMatrix = input.multiMatrix

            elif type(input) == np.ndarray:
                givenMatrix = input

            product = np.matmul(givenMatrix, pVo)
            projectedMatrix = givenMatrix - np.matmul(pUo, product)

            return projectedMatrix
        else:
            # If there is only one dimension,projection is not necessary
            return input.multiMatrix

    def HardThresholdingRank(self):
        """
        sets all but int[rank] biggest singular values to zero
        """
        if self.dim != 1:
            self.fullSingValues[:, self.rank:] = 0
            self.fullU = self.fullU[:,:,:self.rank]

    def calculateSupport(self):
        """
        Output:
            list:support by taking only the int:self.sparsity biggest entries
                         of the frobenius norm of the blocks the current
                         support is calculated
        """
        # Extract the frobenius values of each block and
        # take the absolute value
        frobArray = np.abs(self.fullFrobArray)
        # Sort frobArray by sorting the indices, smalles is on first place
        sortedindex = np.argsort(frobArray)
        # Make biggest entry the first place
        sortedindex = sortedindex[::-1]
        # Cut the indices of to only take the biggest
        support = sortedindex[:self.sparsity]
        return support


    def makeMultiSignalSparsed(self,
                               support,
                               firstTraceOne = False,
                               allTraceOne = False):
        """
        This function sets all blocks to zero which are not in the index
        of np.ndarray:support
        Input:          np.ndarray:support  indices for blocks which are not
                                            set to zero
                        boolean:traceOne    if True, the trace of the first
                                            block of the signal is set to 1
                        boolean:allTraceOne if True, the traces of all blocks
                                            are set to one
        """
        # There must be at least two blocks to threshold the support
        if self.amount != 1:
            # The indices of not active blocks to set them zero
            notSup = np.setdiff1d(np.arange(0, self.amount), np.sort(support))
            # For each index entry in np.ndarray:notSup the corresponding
            # block will be set to zero
            for j in notSup:
                # Unitary U is set to zero
                self.fullU[j] = np.zeros((self.dim,self.rank))
                # If signal is not hermitian, Vdagg is set to idendity matrix
                if not self.hermitian:
                    self.fullVdagg[j] = np.eye(self.dim)
                # before it was = 0 ???
                self.fullSingValues[j] = np.zeros(self.rank)
            # The first signal can be trace normalized
            if firstTraceOne:
                if not np.abs(np.sum(self.fullSingValues[0])) < 1e-15:
                    self.fullSingValues[0] = self.fullSingValues[0] / \
                        np.sum(self.fullSingValues[0])
            # Or all signals can be trace normalized
            if allTraceOne:
                for j in support:
                    if not np.abs(np.sum(self.fullSingValues[j])) < 1e-15:
                        self.fullSingValues[j] = self.fullSingValues[j] / \
                            np.sum(self.fullSingValues[j])
